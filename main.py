from os import name
from PyQt5 import QtWidgets,uic
from PyQt5.QtWidgets import *
import sys
import pymysql

def iniciarconnection():
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 database='prueba',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection    
                                                        
def agregar():
    connection = iniciarconnection()
    print("Hola soy la acción de agregar")
    NAME = ventana.txtNAME.text()
    EMAIL = ventana.txtEMAIL.text()
    BIRTHDATE = ventana.txtBIRTHDATE.text()
    print(NAME,EMAIL,BIRTHDATE)
    sql = f"INSERT INTO Users( NAME, EMAIL ,BIRTHDATE) VALUES (\"{NAME}\",\"{EMAIL}\",\"{BIRTHDATE}\")"
    cursor = connection.cursor()
    cursor.execute(sql)
    connection.commit()
    connection.close()
    consultar()
    
def eliminar():
    connection = iniciarconnection()
    print("Hola soy la acción de eliminar")
    ID = ventana.txtID.text()
    sql = f"DELETE FROM Users WHERE id={ID}"
    cursor = connection.cursor()
    cursor.execute(sql)
    connection.commit()
    connection.close()
    consultar()
    
def modificar():
    connection = iniciarconnection()
    print("Hola soy la acción de modificar")
    NAME = ventana.txtNAME.text()
    EMAIL = ventana.txtEMAIL.text()
    BIRTHDATE = ventana.txtBIRTHDATE.text()
    ID = ventana.txtID.text()
    sql = f"UPDATE Users SET NAME=\"{NAME}\", EMAIL=\"{EMAIL}\",BIRTHDATE=\"{BIRTHDATE}\" WHERE id={ID}"
    cursor = connection.cursor()
    cursor.execute(sql)
    connection.commit()
    connection.close()
    consultar()
    
    
def cancelar():
    print("Hola soy la acción de cancelar")
 
def consultar():
    connection = iniciarconnection()
    ventana.tblContactos.setRowCount(0)
    indiceControl = 0    
    
    sql = "SELECT * FROM `Users`"
    cursor = connection.cursor()
    cursor.execute(sql)
    Users = cursor.fetchall()
    for User in Users:
        ventana.tblContactos.setRowCount(indiceControl+1)
        ventana.tblContactos.setItem(indiceControl,0,QTableWidgetItem(str(User["id"])))
        ventana.tblContactos.setItem(indiceControl,1,QTableWidgetItem(str(User["name"])))
        ventana.tblContactos.setItem(indiceControl,2,QTableWidgetItem(str(User["email"])))
        ventana.tblContactos.setItem(indiceControl,3,QTableWidgetItem(str(User["birthDate"])))
        indiceControl+=1
        
def seleccionar():
    ID =ventana.tblContactos.selectedIndexes()[0].data()
    NAME =ventana.tblContactos.selectedIndexes()[1].data()
    EMAIL =ventana.tblContactos.selectedIndexes()[2].data()
    BIRTHDATE =ventana.tblContactos.selectedIndexes()[3].data()
    print(ID,NAME,EMAIL,BIRTHDATE)
    ventana.txtID.setText(ID)
    ventana.txtNAME.setText(NAME)
    ventana.txtEMAIL.setText(EMAIL)
    ventana.txtBIRTHDATE.setText(BIRTHDATE)
        
aplicacion= QtWidgets.QApplication([])
ventana= uic.loadUi("ventana.ui")
ventana.show()
consultar()

ventana.tblContactos.setHorizontalHeaderLabels(['ID','NAME','EMAIL','BIRTHDATE'])
ventana.tblContactos.setEditTriggers(QTableWidget.NoEditTriggers)
ventana.tblContactos.setSelectionBehavior(QTableWidget.SelectRows)

ventana.tblContactos.cellClicked.connect(seleccionar)

ventana.btnAgregar.clicked.connect(agregar)
ventana.btnEliminar.clicked.connect(eliminar)
ventana.btnModificar.clicked.connect(modificar)
ventana.btnCancelar.clicked.connect(cancelar)

sys.exit(aplicacion.exec())